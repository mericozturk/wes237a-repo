/*
 * EchoAPP -- Echoes on the serial terminal
 */

#include <stdio.h>
#include "platform.h"
#include "xuartps_hw.h"

#define BUF_SIZE 255

int main()
{
	init_platform();

	char buff[BUF_SIZE] = {0};

	int i;
	char c;
	int echoIndex = 0;

	while (1)
	{

		memset(buff, 0, BUF_SIZE);

		print("Enter a string, press Enter to echo (255 max): ");

		for (i=0; i < BUF_SIZE; i++)
		{
			c = XUartPs_RecvByte(STDIN_BASEADDRESS);
			if (c == 13) // enter
			{
				break;
			}
			buff[i] = c;
		}
		print("\n\r");
		for (echoIndex = 0; echoIndex < i; echoIndex++)
		{
			XUartPs_SendByte(STDOUT_BASEADDRESS, buff[echoIndex]);
		}
		print("\n\r");


	}
	return 0;
}
